var _          = require('underscore');
var gulp       = require('gulp');
var del        = require('del');
var path       = require('path');
var newer      = require('gulp-newer');
var rsync      = require('gulp-rsync');
var prompt     = require('gulp-prompt');
var livereload = require('gulp-livereload');

// local config data
var config = require('./config.json');
var includes = config.src + config.includes;

// base rsync options
var rsync_options = _.extend({
	root        : config.src,
	destination : config.dest,
	exclude     : config.excludes,
	progress    : true
}, config.rsync_options);

// the sync
gulp.task('sync', function () {	
	console.log('STARTING SYNC\n=============');
	console.log('Source: ' + includes);
	console.log('Destination: ' + config.dest);

	return gulp.src(includes)
		.pipe(newer(config.dest))
		.pipe(gulp.dest(config.dest));
});

gulp.task('rsync', function () {	
	console.log('STARTING RSYNC\n=============');
	console.log('Source: ' + includes);
	console.log('Destination: ' + config.dest);

	return gulp.src(includes)
		.pipe(rsync(_.extend(rsync_options, {
			incremental : true
		})));
});

gulp.task('full', function () {	
	console.log('FULL SYNC\n=============');
	console.log('Source: ' + config.src);
	console.log('Destination: ' + config.dest);

	return gulp.src(config.src)
		.pipe(prompt.confirm('This will reset all files on the remote and could lead to data loss. Do you wish to continue?'))
		.pipe(rsync(_.extend(rsync_options, {
			clean: true,
			recursive: true
		})));
});

// the watcher
gulp.task('default', function () {
	livereload({ start: true });

	gulp.watch(includes).on('change', function (event) {
		// destination file
		var dest = event.path.replace(config.src, config.dest);

		console.log('[' + event.type.toUpperCase() + '] ' + event.path.replace(config.src + '/', '') + ' => ' + config.dest);

		// nuke it
		var paths = del.sync(dest, { force: true });

		// if its a change, copy it back
		if (event.type === 'changed') {
			gulp.src(event.path)
				.pipe(gulp.dest(path.dirname(dest)));
		}

		// fire livereload with a slight delay to allow the copy to happen
		setTimeout(function () {
			livereload.changed(event.path.replace(config.root, ''));
			if (_.contains(['.css', '.js'], path.extname(event.path))) {
			} else {
				livereload.reload();
			}
		}, config.reload_delay);
	});
});